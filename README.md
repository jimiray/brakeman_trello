## Brakeman Trello

On a Brakeman run for a project or set of projects add cards to the designated Trello list and apply the "Security" label.

Priorities are defined using the confidence value returned by Brakeman as:

High - 1
Mediumn - 2
Low - 3

#### Configuring

- Copy config.yml.example to config.yml
- Obtain Trello developer key and member token and by following the instructions [here](https://github.com/jeremytregunna/ruby-trello#configuration) and enter them in the config.yml file for your project
- Obtain the Trello list id by following the instructuions [here](https://developers.trello.com/get-started/start-building#create) and enter in the config.yml file as list_id for you project
- An example brakeman.yml file can be copied to your application into the config directory.

#### Outputs

Brakeman comparison files are output to the outputs directory as the project_name.json.



